/* src/app.js */

// Styles
import 'Styles/_app.scss';

// import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';

// const screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

$(document).ready(() => {
	require('Scripts/demo');

	// require('Scripts/sliders');

	require('Scripts/scroller');

	// $('.table-tasks').clone(true).appendTo('#table-scroll').addClass('clone');

	// $('.progress-link').each(function() {
	// 	if ($(this).width() > '300') {
	// 		$(this).addClass('longest');
	// 	} else {
	// 		$(this).removeClass('longest');
	// 	}
	// });

	// $('.aside-head').css('height', $('.pageHeader').outerHeight());

	$('.header nav').css('display', 'block');

	$('.js-header-menu-btn').click(function() {
		const id = $(this).data('target');
		$(this).parent().toggleClass('is-open');
		$(id).toggleClass('is-open');
		$('#header-menu-overlay').toggleClass('is-open');
	});
	$('.js-header-menu-close').click(function() {
		$('.c-header-menu').removeClass('is-open');
		$('.js-header-menu-drop').removeClass('is-open');
		$('#header-menu-overlay').removeClass('is-open');
	});
});

// require('Scripts/tabs');
// require('Scripts/calendar');

// load
/*
$(window).on('load', function() {
	if ($('.video-course').length) {
		$('.video-course .scroll-container').mCustomScrollbar({
			axis: screenWidth < 992 ? 'x' : 'y'
		});
	} else {
		$('.scroll-container').mCustomScrollbar();
	}
	window.scrollTo(0, 0);
});

$(window).on('orientationchange', function(e) {
	setTimeout(() => {
		const getWidth = e.currentTarget.outerWidth;
		if ($('.video-course').length) {
			$('.video-course .scroll-container').mCustomScrollbar('destroy', true);
			$('.video-course .scroll-container').mCustomScrollbar({
				axis: getWidth < 992 ? 'x' : 'y'
			});
		}
	}, 0);
});
*/
// load
