import {Calendar} from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import ruLocale from '@fullcalendar/core/locales/ru';
import moment from 'moment';

function setDays(screenWidth) {
	let days;
	if (screenWidth > 991 && screenWidth < 1100) {
		days = 4;
	} else if (screenWidth > 767 && screenWidth < 992) {
		days = 3;
	} else if (screenWidth < 768) {
		days = 1;
	} else {
		days = 7;
	}

	return days;
}

let calendar;

document.addEventListener('DOMContentLoaded', function() {
	const screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	const calendarEl = document.getElementById('calendar');
	const calendarMain = document.getElementsByClassName('calendar--main');
	const isCalendarMain = !!calendarMain[0];
	if (!calendarEl) {
		return;
	}
	let getView = calendarEl.dataset.view;
	const getMobileView = calendarEl.dataset.mobileView;
	const getHeightMain = calendarEl.dataset.height;
	const events = calendarEl.dataset.events;
	// start time "2020-09-01T08:30:00"

	if (screenWidth < 1100) {
		getView = getMobileView;
	}

	calendar = new Calendar(calendarEl, {
		plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin], // timeGridWeek
		locales: [ruLocale],
		locale: 'ru',
		firstDay: 1,
		timeZone: 'local',
		height: parseFloat(getHeightMain),
		initialView: getView,
		headerToolbar: {
			left: 'prev',
			center: 'title',
			right: 'next',
		},
		slotMinTime: isCalendarMain ? '08:00:00' : '00:00:00',
		slotMaxTime: isCalendarMain ? '21:00:00' : '24:00:00',
		slotLabelFormat: {
			hour: '2-digit',
			minute: '2-digit',
			omitZeroMinute: false,
			meridiem: 'short'
		},
		views: {
			dayGridWeek: {
				titleFormat: {year: 'numeric', month: 'long'}
			},
			timeGridWeek: {
				titleFormat: {year: 'numeric', month: 'long'}
			},
			dayGrid: {
				titleFormat: {year: 'numeric', month: 'long'}
			},
			listMonth: {
				titleFormat: {year: 'numeric', month: 'long'}
			},
		},
		allDaySlot: false,
		duration: {days: setDays(screenWidth)},
		// allDayText: 'весь день',
		// moreLinkText: 'больше',
		// noEventsText: 'Нет событий для отображения',
		events: JSON.parse(events),
		dayHeaderDidMount: function(args) {
			const day = moment(args.date).locale('ru').format('DD');
			const dayName = moment(args.date).locale('ru').format('ddd');
			const fullDayName = moment(args.date).locale('ru').format('dddd');
			const getHeader = args.el.querySelector('.fc-col-header-cell-cushion');
			const getListHeader = args.el.querySelector('.fc-cell-shaded');
			const isWeek = args.el.dataset.date;
			if (getListHeader) {
				getListHeader.innerHTML = `<div class="fc-list-day-text">${day}</div> <div class="fc-list-day-side-text">${fullDayName}</div>`;
			}
			if (getHeader && isWeek) getHeader.innerHTML = `<p>${day}</p> ${dayName}`;
		},
		eventDidMount: function(info) {
			const div = document.createElement('div');
			const getEvent = info.el.querySelector('.fc-event-title');
			const getListEvent = info.el.querySelector('.fc-list-event-title');
			if (isCalendarMain) {
				const description = info.event.extendedProps.description;
				const ico = info.event.extendedProps.ico;
				const dates = info.event.extendedProps.dates;
				const title = info.event.extendedProps.titles;
				const setIco = ico ? `<div class="calendar-ico"><img src=${ico} alt="" /></div>` : '';
				const setTitle = title ? `<div class="calendar-title">${title}</div>` : '';
				const setDates = dates ? `<div class="calendar-dates">${dates}</div>` : '';
				const setDescription = description ? `<div class="calendar-description">${description}</div>` : '';
				if (getEvent) {
					$(getEvent)
						.html(`<div class="calendar-up">${setIco}${setTitle}</div><div class="calendar-down">${setDates}${setDescription}</div>`);
				}
				if (getListEvent) {
					$(getListEvent)
						.html(`<div class="calendar-up">${setIco}${setTitle}</div><div class="calendar-down">${setDates}${setDescription}</div>`);
				}
			} else {
				div.innerHTML = info.event.extendedProps.description;
				if (getEvent) getEvent.appendChild(div);
				if (getListEvent) getListEvent.appendChild(div);
			}
		},
		datesSet: function(info) {
			const getCurrentDate = info.view.getCurrentData().currentDate;
			const getPrevDate = moment(getCurrentDate).subtract(1, 'M').startOf('month').locale('ru').format('MMMM');
			const getNextDate = moment(getCurrentDate).add(1, 'M').startOf('month').locale('ru').format('MMMM');
			$('.fc-button-next span').text(getNextDate);
			$('.fc-button-prev span').text(getPrevDate);
		}
	});
	let getCurrentDate = moment();
	$('.fc-button-prev span').text(moment().subtract(1, 'M').startOf('month').locale('ru').format('MMMM'));
	$('.fc-button-next span').text(moment().add(1, 'M').startOf('month').locale('ru').format('MMMM'));
	$('.fc-button-today span').text(moment().locale('ru').format('DD MMMM YYYY'));


	$('.fc-button-prev img').click(function() {
		const getPrevDate = getCurrentDate.subtract(1, 'M').startOf('month').locale('ru');
		const getPrevDateFormat = getPrevDate.format();
		calendar.gotoDate(getPrevDateFormat);
		getCurrentDate = moment(getPrevDateFormat);
	});
	$('.fc-button-next img').click(function() {
		const getNextDate = getCurrentDate.add(1, 'M').startOf('month').locale('ru');
		const getNextDateFormat = getNextDate.format();
		calendar.gotoDate(getNextDateFormat);
		getCurrentDate = moment(getNextDateFormat);
	});

	calendar.render();

	$('.calendar-links--js a').click(function() {
		calendar.destroy();
		$('.calendar-links--js a').removeClass('active');
		const getType = $(this).data('type');
		const getHeight = $(this).data('height');
		$(this).addClass('active');

		if (getType !== 'listMonth') {
			$('.calendar-helper').show();
		} else {
			$('.calendar-helper').hide();
		}

		if ((getType === 'dayGridMonth' || getType === 'dayGridWeek') && screenWidth < 1100) {
			calendar.changeView('dayGrid');
			calendar.setOption('height', 360);
		} else {
			calendar.changeView(getType);
			calendar.setOption('height', getHeight);
		}

		calendar.render();
		return false;
	});
});

function setOptions(event) {
	const screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	const calendarMain = document.getElementsByClassName('calendar--main');
	const getWidth = event.target.screen.width;

	if(calendar) {
		calendar.destroy();

		if (!!calendarMain) {
			if (screenWidth < 1100) calendar.changeView('listMonth');
			else calendar.changeView('timeGridWeek');
		}
		calendar.setOption('duration', {days: setDays(getWidth)});
		calendar.render();
	}
}

window.addEventListener('orientationchange', setOptions);
window.addEventListener('resize', setOptions);
