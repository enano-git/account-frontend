function tabControl() {
	const tabs = $('.tabbed-content').find('.tabs');
	if (tabs.is(':visible')) {
		tabs.find('a').on('click', function(event) {
			event.preventDefault();
			const target = $(this).attr('href');
			const tab = $(this).parents('.tabs');
			const buttons = tab.find('a');
			const item = tab.parents('.tabbed-content').find('.item');
			buttons.removeClass('active');
			item.removeClass('active');
			$(this).addClass('active');
			$(target).addClass('active');
		});
	} else {
		$('.item').on('click', function() {
			const container = $(this).parents('.tabbed-content');
			const currId = $(this).attr('id');
			const items = container.find('.item');
			container.find('.tabs a').removeClass('active');
			items.removeClass('active');
			$(this).addClass('active');
			container.find('.tabs a[href$="#' + currId + '"]').addClass('active');
		});
	}
}

$(document).ready(() => {
	tabControl();
});

let resizeTimer;
$(window).on('resize', function() {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function() {
		tabControl();
	}, 250);
});
