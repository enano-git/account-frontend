// anchor
// $('.anchor').on('click', 'a', function(event) {
// 	event.preventDefault();
// 	const id = $(this).attr('href');
// 	const top = $(id).offset().top;
// 	$('body,html').animate({scrollTop: top + 40}, 1000);
// });
// anchor

// tabs
// $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
// 	$(this)
// 		.addClass('active').siblings().removeClass('active')
// 		.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
// });
// tabs

// accordion
$('.accordion-header, .main-nav__item--parent > a').click(function(e) {
	e.preventDefault();
	// $(this).toggleClass('active');
	$(this).toggleClass('opened');
	// $(this).next().slideToggle();
});
// accordion

// video
// $('.webinar-play--js').click(function() {
// 	$(this).fadeOut();
// 	$(this).next()[0].play();
// });
// video

// $('.showmore--js').click(function() {
// 	$(this).hide().parent().addClass('shown');
// 	return false;
// });

// $('.input__file-js').change(function() {
// 	const name = this.value;
// 	const reWin = /.*\\(.*)/;
// 	let fileTitle = name.replace(reWin, '$1');
// 	const reUnix = /.*\/(.*)/;
// 	fileTitle = fileTitle.replace(reUnix, '$1');
// 	$(this).closest('.messages-bottom').addClass('messages-bottom--text');
// 	$(this).parent().next().removeClass('error-text').text(fileTitle);
// });


// adaptive menu
$('.main-nav__toggle--js').click(function() {
	$(this).next().slideToggle();
});
$('.search--js').click(function() {
	$(this).parent().addClass('active');
});
$('.header-menu--js').click(function() {
	$('.header-menu--js').toggleClass('active');
	$('body').toggleClass('is-panel-opened');
	// $('main').toggleClass('active');
	// $('.aside').toggleClass('active');
	// $('.footer').toggleClass('active');
	return false;
});
$('.l-panel-overlay').click(function() {
	$('.header-menu--js').removeClass('active');
	$('body').removeClass('is-panel-opened');
});
$('.c-panel-title--js').click(function() {
	$(this).toggleClass('active');
	$(this).next().slideToggle('500');
});
// adaptive menu
