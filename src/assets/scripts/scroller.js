let getScrollPosition = 0;

$('.table--responsive').scroll(function() {
	getScrollPosition = $('.table--responsive').scrollLeft();
});

$('.scroll-prev').click(function() {
	if (getScrollPosition <= 0) {
		getScrollPosition = 0;
		return;
	}
	getScrollPosition -= 200;
	$('.table--responsive').animate({'scrollLeft': getScrollPosition}, 500);
});
$('.scroll-next').click(function() {
	const getMaxScrollPosition = $('.table--responsive')[0].scrollWidth - $('.table--responsive').width();
	if (getMaxScrollPosition <= getScrollPosition) {
		getScrollPosition = getMaxScrollPosition;
		return;
	}
	getScrollPosition += 200;
	$('.table--responsive').animate({'scrollLeft': getScrollPosition}, 500);
});
